import os

import psycopg2
from psycopg2.extras import RealDictCursor

INIT_SQL_FILENAME = "__init.sql"
SQLPATCH_FILENAME = "sqlpatch/ver_{version}.sql"
PG_USERNAME = os.getenv("PG_USERNAME")
PG_PASSWORD = os.getenv("PG_PASSWORD")


def get_conn():
    conn = psycopg2.connect(
        user=PG_USERNAME,
        password=PG_PASSWORD,
        host="127.0.0.1",
        port="5432",
        database="snippet",
        cursor_factory=RealDictCursor,
    )
    return conn


def init_db():
    """Runs the content of <INIT_SQL_FILENAME>"""
    with open(INIT_SQL_FILENAME, "r") as f:
        lines = " ".join(f.readlines())

    with get_conn() as conn:
        cursor = conn.cursor()
        cursor.execute(lines)

    conn.close()


def run_sqlpatches():
    """Runs the appropriate upgrade scripts based on the current schemaversion
    of the database."""
    with get_conn() as conn:
        cursor = conn.cursor()
        cursor.execute("SELECT schemaversion FROM meta")
        db_version = cursor.fetchone().get("schemaversion", 1) + 1

        while os.path.exists(SQLPATCH_FILENAME.format(version=db_version)):
            with open(SQLPATCH_FILENAME.format(version=db_version), "r") as f:
                lines = " ".join(f.readlines())
            cursor.execute(lines)
            conn.commit()
            print(f'Applied sqlpatch version {db_version}')
            db_version += 1

    conn.close()


if __name__ == "__main__":
    """Run this file independently to create the initial schema."""
    init_db()
    print("Successfully run init db script")

    run_sqlpatches()
