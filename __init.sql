CREATE TABLE IF NOT EXISTS meta (
    schemaversion INTEGER
);

CREATE TABLE IF NOT EXISTS snippets (
    id SERIAL NOT NULL,
    name TEXT NOT NULL,
    snippet_text TEXT NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS variables (
    id SERIAL NOT NULL,
    name TEXT NOT NULL,
    default_value TEXT NOT NULL,
    snippet_id INTEGER REFERENCES snippets(id) ON DELETE CASCADE,
    PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS tags (
    id SERIAL NOT NULL,
    name TEXT NOT NULL UNIQUE,
    PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS snippet_tag_links (
    snippet_id INTEGER REFERENCES snippets(id) ON DELETE CASCADE,
    tag_id INTEGER REFERENCES tags(id),
    PRIMARY KEY (snippet_id, tag_id)
);

CREATE OR REPLACE FUNCTION generate_snippet_text(
    _snippet_id INTEGER
) RETURNS TEXT AS $$
DECLARE
    r           RECORD;
    i           INTEGER := 1;
    span_text   TEXT;
    orig_text   TEXT;
BEGIN
    SELECT      snippet_text
    INTO        orig_text
    FROM        snippets
    WHERE       id = _snippet_id;
    --
    FOR r IN (
        SELECT      name
        FROM        variables
        WHERE       snippet_id = _snippet_id
        ORDER BY    id ASC
    )
    LOOP
        span_text := CONCAT('<span x-text="', r.name, '"></span>');
        SELECT  REPLACE (orig_text, CONCAT('$', i::TEXT), span_text)
        INTO    orig_text;
        i = i + 1;
    END LOOP;
    --
    RETURN orig_text;
END;
$$ LANGUAGE 'plpgsql';

DROP VIEW IF EXISTS tag_counts;
CREATE VIEW tag_counts AS
    SELECT      name,
                COUNT(snippet_id)
    FROM        tags
    INNER JOIN  snippet_tag_links ON tags.id = snippet_tag_links.tag_id
    GROUP BY    id
    ORDER BY    COUNT(snippet_id) DESC
    LIMIT       20;

DROP VIEW IF EXISTS snippets_list;
CREATE VIEW snippets_list AS
    WITH tag_details AS (
        SELECT      stl.snippet_id,
                    JSON_AGG(JSON_BUILD_OBJECT('id', id, 'name', name)) AS tags
        FROM        tags
        INNER JOIN  snippet_tag_links stl ON tags.id = stl.tag_id
        GROUP BY    stl.snippet_id
    ), variable_details AS (
        SELECT      snippet_id,
                    JSON_OBJECT_AGG(name, default_value) AS vars
        FROM        variables
        GROUP BY    snippet_id
    )
    SELECT      snippets.id,
                snippets.name,
                generate_snippet_text(snippets.id) AS snippet_text,
                COALESCE(td.tags, '[]'::JSON) AS tags,
                COALESCE(vd.vars, '[]'::JSON) AS vars
    FROM        snippets
    LEFT JOIN   tag_details td ON td.snippet_id = snippets.id
    LEFT JOIN   variable_details vd ON vd.snippet_id = snippets.id
    ORDER BY    snippets.id DESC;

CREATE OR REPLACE FUNCTION get_snippets_by_search_query(
    _search_query TEXT, _search_tsquery TEXT
) RETURNS TABLE (
    id INTEGER,
    name TEXT,
    snippet_text TEXT,
    tags JSON,
    vars JSON
) AS $$
BEGIN
    RETURN QUERY
    SELECT      *
    FROM        snippets_list
    WHERE       snippets_list.id IN (
        SELECT      snippet_id AS id
        FROM        snippet_tag_links stl
        INNER JOIN  tags ON tags.id = stl.tag_id
        WHERE       _search_query IS NOT NULL
                    AND (
                        tags.name = _search_query
                        OR tags.name = CONCAT('#', _search_query)
                    )
        --
        UNION
        --
        SELECT      snippets.id
        FROM        snippets
        WHERE       _search_query IS NOT NULL
                    AND snippets.textsearchable_index_col @@ to_tsquery(_search_tsquery)
    );
END;
$$ LANGUAGE 'plpgsql';

CREATE OR REPLACE FUNCTION get_snippets_by_tagname(
    _tag_name TEXT
) RETURNS TABLE (
    id INTEGER,
    name TEXT,
    snippet_text TEXT,
    tags JSON,
    vars JSON
) AS $$
BEGIN
    RETURN QUERY
        SELECT  *
        FROM    snippets_list
        WHERE   snippets_list.id IN (
            SELECT  snippet_id
            FROM    snippet_tag_links
            WHERE   tag_id IN (
                        SELECT  tags.id
                        FROM    tags
                        WHERE   tags.name = _tag_name
                    )
        );
END;
$$ LANGUAGE 'plpgsql';

CREATE OR REPLACE FUNCTION create_snippet(
    snippet_data JSONB
) RETURNS INTEGER AS $$
DECLARE
    snippet_id          INTEGER;
    r                   RECORD;
BEGIN
    WITH tag_names AS (
        SELECT          word
        FROM            (SELECT UNNEST(string_to_array(snippet_data->>'name', ' ')) word) t
        WHERE           t.word LIKE '#%'
    ), tags_insert AS (
        INSERT INTO     tags (name)
        SELECT          word
        FROM            tag_names
        ON CONFLICT     DO NOTHING
        RETURNING       id
    ), tag_ids AS (
        SELECT          id
        FROM            tags
        WHERE           name IN (SELECT word FROM tag_names)
        UNION
        SELECT          id
        FROM            tags_insert
    ), snippets_insert AS (
        INSERT INTO     snippets
                        (name, snippet_text)
        VALUES          (REGEXP_REPLACE(REGEXP_REPLACE(snippet_data->>'name', '#\w+', '', 'gi'), '\s+', ' ', 'g'),
                        snippet_data->>'snippet')
        RETURNING       id
    ),
    snippet_tag_links_insert AS (
        INSERT INTO     snippet_tag_links
                        (snippet_id, tag_id)
        SELECT          snippet.id, tag.id
        FROM            snippets_insert snippet, tag_ids tag
    ),
    variables_insert AS (
        INSERT INTO     variables (name, default_value, snippet_id)
        SELECT          REPLACE(vars.elem->>'name', ' ', '_'),
                        vars.elem->>'value',
                        snippets_insert.id
        FROM            (SELECT jsonb_array_elements((snippet_data->>'variables')::JSONB) elem) vars,
                        snippets_insert
    )
    SELECT id INTO snippet_id
    FROM snippets_insert;
    --
    RETURN snippet_id;
END;
$$ LANGUAGE 'plpgsql';

-- SELECT create_snippet('{"name":"title #twitch #awesome","variables":[{"name":"hey","value":"yo"}],"snippet":"cool snippet text content $1"}');
