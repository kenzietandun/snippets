const createSnippet = (payload) => {
  fetch("{{ url_for('create_snippet') }}", {
    method: "POST",
    headers: { "Content-Type": "application/json" },
    body: payload,
  })
    .then((resp) => resp.json())
    .then((data) => window.location.replace(data.redirect))
    .catch((err) => console.log(err));
};
