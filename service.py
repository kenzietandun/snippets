import html
import json

from db import get_conn


def _cleanup_snippet_text(text):
    lines = text.split("\n")
    lines_leading_whitespaces = [len(line) - len(line.lstrip()) for line in lines]
    num_whitespaces_to_remove = min(lines_leading_whitespaces)
    lines = [line[num_whitespaces_to_remove:] for line in lines]
    text = "\n".join(lines)

    text = html.escape(text)

    return text


def add_snippet(snippet_json):
    snippet_text = snippet_json.get("snippet", "")
    snippet_json["snippet"] = _cleanup_snippet_text(snippet_text)
    snippet_json = json.dumps(snippet_json)

    conn = get_conn()
    cursor = conn.cursor()

    snippet_id = None

    try:
        cursor.execute(
            " SELECT create_snippet(%s::JSONB) AS id",
            (snippet_json,),
        )
        conn.commit()
        snippet_id = cursor.fetchone()
    finally:
        cursor.close()
        conn.close()

    return snippet_id.get("id")


def search_snippets_by_text_query(query):
    tsquery = " & ".join(query.split(" "))

    conn = get_conn()
    cursor = conn.cursor()

    results = []

    try:
        cursor.execute(
            "SELECT * FROM get_snippets_by_search_query(%s, %s)",
            (
                query,
                tsquery,
            ),
        )
        conn.commit()
        results = cursor.fetchall()
    finally:
        cursor.close()
        conn.close()

    return results


def read_snippet_by_id(snippet_id: int):
    conn = get_conn()
    cursor = conn.cursor()

    result = None

    try:
        cursor.execute(
            " SELECT * FROM snippets_list WHERE id = %s ",
            (snippet_id,),
        )
        result = cursor.fetchone()
    finally:
        cursor.close()
        conn.close()
    return result


def read_snippets_by_tagname(tag_name):
    conn = get_conn()
    cursor = conn.cursor()

    results = []

    try:
        cursor.execute(
            " SELECT * FROM get_snippets_by_tagname(%s) ",
            (tag_name,),
        )
        results = cursor.fetchall()
    finally:
        cursor.close()
        conn.close()
    return results


def read_tag_counts():
    conn = get_conn()
    cursor = conn.cursor()

    results = []

    try:
        cursor.execute("SELECT * FROM tag_counts")
        results = cursor.fetchall()
    finally:
        cursor.close()
        conn.close()
    return results


def read_last_n_snippets(n):
    conn = get_conn()
    cursor = conn.cursor()

    results = []

    try:
        cursor.execute(
            " SELECT * FROM snippets_list LIMIT %s ",
            (n,),
        )
        results = cursor.fetchall()
    finally:
        cursor.close()
        conn.close()
    return results
