const copyToClipboard = (cssSelector) => {
  const copyText = document.querySelector(`${cssSelector} pre`);
  if (navigator.clipboard && window.isSecureContext) {
    navigator.clipboard.writeText(copyText.textContent);

    const copyBtn = document.querySelector(`${cssSelector} button`);
    copyBtn.innerText = "Copied";
  }
};
