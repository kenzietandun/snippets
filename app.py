from flask import Flask, jsonify, redirect, render_template, request, url_for
from flask_cors import CORS

import service

app = Flask(__name__)

CORS(app)


def _is_api_call(request_path):
    return request_path.startswith("/api")


@app.route("/")
def index():
    return render_template("index.html", tag_counts=service.read_tag_counts())


@app.route("/create", methods=["GET"])
def create():
    return render_template("create.html")


@app.route("/api/search", methods=["GET"])
@app.route("/search", methods=["GET"])
def get_snippets_by_search_query():
    query = request.args.to_dict().get("q", "")
    if query:
        snippets = service.search_snippets_by_text_query(query)
        if _is_api_call(request.path):
            return jsonify(snippets=snippets)
        return render_template(
            "index.html",
            snippets=snippets,
            tag_counts=service.read_tag_counts(),
        )
    else:
        return redirect(url_for("index"))


@app.route("/api/tags/<tag_name>", methods=["GET"])
@app.route("/tags/<tag_name>", methods=["GET"])
def get_snippets_by_tagname(tag_name):
    snippets = service.read_snippets_by_tagname(tag_name)
    if _is_api_call(request.path):
        return jsonify(snippets=snippets)
    else:
        return render_template(
            "index.html", snippets=snippets, tag_counts=service.read_tag_counts()
        )


@app.route("/snippets", methods=["POST"])
def create_snippet():
    snippet_id = service.add_snippet(request.get_json())
    return jsonify(redirect=url_for("get_snippet_by_id", snippet_id=snippet_id))


@app.route("/api/snippets/<int:snippet_id>", methods=["GET"])
@app.route("/snippets/<int:snippet_id>", methods=["GET"])
def get_snippet_by_id(snippet_id):
    snippet = service.read_snippet_by_id(snippet_id)
    if _is_api_call(request.path):
        return jsonify(snippet=snippet)
    return render_template(
        "index.html", snippet=snippet, tag_counts=service.read_tag_counts()
    )
