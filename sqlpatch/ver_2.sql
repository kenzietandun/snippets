ALTER TABLE         snippets
ADD COLUMN          textsearchable_index_col tsvector
GENERATED ALWAYS AS (to_tsvector('english', COALESCE(name, ''))) STORED;

CREATE INDEX        textsearch_idx
ON                  snippets
USING               GIN (textsearchable_index_col);

UPDATE meta SET schemaversion = 2;
